package com.utils.selector;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.cucumber.java.en.And;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.By.xpath;


public class LoginPage {
    private AppiumDriver driver = new DriverManager().getDriver();

    public LoginPage(AppiumDriver driver) {
        this.driver = new DriverManager().getDriver();
    }

    private boolean isElementPresent(WebElement locator) {
        try {
            driver.findElement((By) locator);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @AndroidFindBy (xpath = "//android.view.View[@content-desc=\"Welcome to HRIS\"]")
    private WebElement LoginTitle;
    @AndroidFindBy (xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText[1]")
    private WebElement UsernameTxtFld;
    @AndroidFindBy (xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText[2]")
    private WebElement PasswordTxtFld;
    @AndroidFindBy (xpath = "//android.widget.Button[@content-desc=\"Sign In\"]")
    private WebElement LoginBtn;
    @AndroidFindBy (xpath = "//android.view.View[@content-desc=\"Invalid Credential\"]")
    private WebElement ErrAlert;
    public LoginPage(){

    }
    public boolean CheckPage(){
        if (driver != null && driver.getSessionId() != null) {
            System.out.println("Driver is valid and has a session ID: " + driver.getSessionId().toString());
        } else {
            System.out.println("Driver is not valid or does not have a session ID.");
        }

        if (LoginTitle.isDisplayed()) {
            System.out.println("Element exists");
            return true;
        } else {
            System.out.println("Element does not exist");
            return false;
        }
    }
    public LoginPage EnterUsername(String username){
        UsernameTxtFld.sendKeys(username);
        return this;
    }

}
