package com.utils.selector;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;

public class DriverManager {
    private static ThreadLocal<AppiumDriver> driver = new ThreadLocal<>();

    public AppiumDriver getDriver(){
        return driver.get();
    }

    public void initializeDriver() throws Exception {
        AppiumDriver driver = null;
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Rama1");
        caps.setCapability("appWaitActivity", "com.swaglabsmobileapp.MainActivity");
        String appUrl = System.getProperty("user.dir")
                + File.separator + "src" +File.separator + "main" +File.separator + "resources"
                +File.separator + "Android.SauceLabs.Mobile.Sample.app.2.7.1.apk";
        caps.setCapability(MobileCapabilityType.APP, appUrl);

        URL url = new URL("http://0.0.0.0:4723");
        driver = new AndroidDriver(url, caps);

        /*if(driver == null){
            try{
                DesiredCapabilities caps = new DesiredCapabilities();
                caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
                caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
                caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Rama1");
                caps.setCapability("appWaitActivity", "com.swaglabsmobileapp.MainActivity");
                String appUrl = System.getProperty("user.dir")
                        + File.separator + "src" +File.separator + "main" +File.separator + "resources"
                        +File.separator + "Android.SauceLabs.Mobile.Sample.app.2.7.1.apk";
                caps.setCapability(MobileCapabilityType.APP, appUrl);

                URL url = new URL("http://0.0.0.0:4723");
                driver = new AndroidDriver(url, caps);
//                Thread.sleep(Duration.ofSeconds(5));

                if(driver == null){
                    throw new Exception("driver is null. ABORT!!!");
                }
                this.driver.set(driver);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Driver initialization failure. ABORT !!!!" + e.toString());
                throw e;
            }
        }*/

    }
}
