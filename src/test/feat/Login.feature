Feature: Login Scenarios

  Scenario Outline: Login with invalid user name
    Given I am on login page
    When I enter invalid username "<username>"
    And I enter invalid password "<password>"
    And I click login
    Then I should get error message "<err>"
    Examples:
      | username | password | err |
      | rama1 | secret_sauce | Username and password do not match any user in this service. |