package com.rama.stepdef;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Driver;
import java.time.Duration;

import com.utils.selector.DriverManager;


public class LoginStepDef {
   /* @AndroidFindBy(xpath = "//android.view.View[@content-desc=\"Welcome to HRIS\"]")
    private WebElement LoginTitle;
    @AndroidFindBy (xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText[1]")
    private WebElement UsernameTxtFld;
    @AndroidFindBy (xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText[2]")
    private WebElement PasswordTxtFld;
    @AndroidFindBy (xpath = "//android.widget.Button[@content-desc=\"Sign In\"]")
    private WebElement LoginBtn;
    @AndroidFindBy (xpath = "//android.view.View[@content-desc=\"Invalid Credential\"]")
    private WebElement ErrAlert;*/

    private AppiumDriver driver;

    @Before
    public void callDriver() {
        DriverManager LetsGoDriver = new DriverManager();
        driver = LetsGoDriver.getDriver();

        /*if (driver != null && driver.getSessionId() != null) {
            System.out.println("Driver is valid and has a session ID: " + driver.getSessionId().toString());
        } else {
            System.out.println("Driver is not valid or does not have a session ID.");
        }*/
    }

    /*@BeforeClass
    public static void initialize() throws Exception {
        DriverManager LetsgoDriver = new DriverManager();
        LetsgoDriver.initializeDriver();
        Thread.sleep(Duration.ofSeconds(5));
    }*/

    /*@Before
    public void initialize() throws MalformedURLException, InterruptedException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Rama1");
        caps.setCapability("appWaitActivity", "com.swaglabsmobileapp.MainActivity");
        String appUrl = System.getProperty("user.dir")
                + File.separator + "src" +File.separator + "main" +File.separator + "resources"
                +File.separator + "Android.SauceLabs.Mobile.Sample.app.2.7.1.apk";
        caps.setCapability(MobileCapabilityType.APP, appUrl);

        URL url = new URL("http://0.0.0.0:4723");
        driver = new AndroidDriver(url, caps);
        Thread.sleep(Duration.ofSeconds(5));
    }*/

    @Given("I am on login page")
    public void iAmOnLoginPage() {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("Given");

        /*if (driver != null && driver.getSessionId() != null) {
            System.out.println("Driver is valid and has a session ID: " + driver.getSessionId().toString());
        } else {
            System.out.println("Driver is not valid or does not have a session ID.");
        }*/

        WebElement LoginBtnHome = driver.findElement(AppiumBy.accessibilityId("test-LOGIN"));

        // Check if the LoginBtnHome is displayed
        boolean isDisplayed = LoginBtnHome.isDisplayed();
        if (isDisplayed) {
            System.out.println("Login button is displayed.");
        } else {
            System.out.println("Login button is not displayed.");
        }
    }

    @When("I enter invalid username {string}")
    public void iEnterInvalidUsername(String username) {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("When1");

        WebElement UsernameTxt = driver.findElement(AppiumBy.accessibilityId("test-Username"));
        UsernameTxt.sendKeys(username);

        // Check value
        String UsernameTxtValue = UsernameTxt.getAttribute("text");
        System.out.println("Username: " + UsernameTxtValue);
    }

    @When("I enter invalid password {string}")
    public void iEnterInvalidPassword(String password) {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("When2");

        WebElement PasswordTxt = driver.findElement(AppiumBy.accessibilityId("test-Password"));
        PasswordTxt.sendKeys(password);

        // Check value
        String PasswordTxtValue = PasswordTxt.getAttribute("text");
        System.out.println("Password: " + PasswordTxtValue);
    }

    @When("I click login")
    public void iClickLogin() {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("When3");

        WebElement LoginBtn = driver.findElement(AppiumBy.accessibilityId("test-LOGIN"));
        LoginBtn.click();
    }

    @Then("I should get error message {string}")
    public void i_should_get_error_message_username_and_password_do_not_match_any_user_in_this_service(String err) {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("Then");

        WebElement ErrText = driver.findElement(AppiumBy.xpath("//android.view.ViewGroup[@content-desc=\"test-Error message\"]/android.widget.TextView"));
        Assert.assertEquals(err, ErrText.getText());
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
