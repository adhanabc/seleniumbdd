package com.rama.stepdef;

import com.utils.selector.DriverManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

public class Hooks {
    private AppiumDriver driver;

    @BeforeClass
    public static void initialize() throws Exception {
        DriverManager LetsgoDriver = new DriverManager();
        LetsgoDriver.initializeDriver();
        Thread.sleep(Duration.ofSeconds(5));

        AppiumDriver driver = LetsgoDriver.getDriver();

        if (driver != null && driver.getSessionId() != null) {
            System.out.println("Driver is valid and has a session ID: " + driver.getSessionId().toString());
        } else {
            System.out.println("Driver is not valid or does not have a session ID.");
        }
    }

    /*@Before
    public void initialize() throws MalformedURLException, InterruptedException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Rama1");
        String appUrl = System.getProperty("user.dir")
                + File.separator + "src" +File.separator + "main" +File.separator + "resources"
                +File.separator + "218caf00-6fa5-47c8-9a9f-4bf1bc1d6006.apk";
        caps.setCapability(MobileCapabilityType.APP, appUrl);

        URL url = new URL("http://0.0.0.0:4723");
        driver = new AndroidDriver(url, caps);
        this.driver = new DriverManager().getDriver();
        Thread.sleep(Duration.ofSeconds(5));
    }

    @After
    public void tearDown(){
        if (driver != null) {
            driver.quit();
        }
    }*/
}
