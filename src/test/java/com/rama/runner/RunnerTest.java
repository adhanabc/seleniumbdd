package com.rama.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import static io.cucumber.junit.CucumberOptions.*;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber","summary"},
        features = {"src/test/resources"},
        glue = {"com.rama.stepdef"},
        snippets = SnippetType.CAMELCASE,
        dryRun = false,
        monochrome = true

)

public class RunnerTest  {
}
